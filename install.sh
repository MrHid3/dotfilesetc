#!/bin/bash
set -e 

read -p "Are you sure you want to procede? This script will most likely override many files on your system [y/n] " yn

if [ $yn != "y" ]; then
	echo "Aborting..."
	exit 0
fi
read -p "Your home directory (for example /home/johnsmith, notice there is no backslash at the end) : " homepath

if [ ! -d $homepath ]; then
	echo "Not a valid home directory. Aborting..."
	exit 0
fi

read -p "Are you sure $homepath is where you want to install this configuration? It may overwrite many files [y/n] " yn

if [ $yn != "y" ]; then
	echo "Aborting..."
	exit 0
fi

echo "Initiating package installation..."
sudo pacman -S i3 pavucontrol pipewire pipewire-pulse pipewire-alsa pipewire-media-session pipewire-audio xrandr picom firefox steam discord spotify-launcher --needed
echo "Package installation succesfull!"

echo "Initiating configuration installation..."

echo "Copying .xinitrc to $homepath"
cp configs/.xinitrc $homepath/
echo ".xinitrc (X server configuration file) copied succesfully"

echo "Copying .bashrc to to $homepath"
cp configs/.bashrc $homepath/
echo ".bashrc copied succesfully!"

echo "Copying .bash_profile to $homepath"
cp configs/.bash_profile $homepath/
echo ".bash_profile copied succesfully!"

echo "Copying picom.conf to $homepath/.config"
cp configs/picom.conf $homepath/
echo "picom.conf (X11/ compositor configuration file) copied succesfully!"

echo "Copying .zshrc to $homepatch/.zshrc"
cp configs/.zshrc $homepath/
echo ".zshrc copied succesfully!"

echo "Copying wallpaper.jpg to $homepath/Pictures"
if [ ! -d $homepath/Pictures ]; then
	mkdir $homepath/Pictures
	echo "Created directory $homepath/Pictures"
fi
if [ ! -f $homepath/Pictures/wallpaper.jpg ]; then
	cp pictures/wallpaper.jpg $homepath/Pictures/
	echo "wallpaper.jpg copied succesfully!"
else
	echo "skipping copying wallpaper.jpg..."
fi

echo "Copying config to $homepath/.config/i3"
if [ ! -d "$homepath/.config/i3" ]; then
	if [ ! -d "$homepath/.config" ]; then
		mkdir $homepath/.config
		echo "Created directory $homepath/.config"
	fi
	mkdir $homepath/.config/i3
	echo "Created directory $homepath/.config/i3"
fi

cp configs/config $homepath/.config/i3/
echo "config (i3 configuration file) copied succesfully!"

echo "Copying .i3status.conf to $homepath"
cp configs/.i3status.conf  $homepath/
echo ".i3status.config (i3bar configuration file) copied succesfully!"

echo "Copying .i3blocks.conf to $homepath"
cp configs/.i3blocks.conf  $homepath/
echo ".i3blocks.conf (i3bar second monitor configuration file) copied succesfully!"


read -p "Do you want to modify your grub config? [y/n] " yn

if [ $yn == "y" ]; then
	echo "Copying grub to /etc/default/grub"
	sudo cp configs/grub /etc/default/
	echo "grub (grub configuration file) copied succesfully!"
	echo "Updating grub..."
	sudo update-grub
	echo "Grub updated succesfully!"
else 
	echo "Skipping...";
fi


echo "Cloning github.com/mrhid3/i3lock-color..."
git clone https://github.com/mrhid3/i3lock-color
echo "Running i3lock-color/install-i3lock-color.sh..."
cd i3lock-color
./install-i3lock-color.sh
cd ..
rm i3lock-color -rf
echo "i3lock-color installated succesfully!"

echo "Cloning github.com/mrhid3/st..."
git clone https://github.com/mrhid3/st
echo "Running sudo make clean install..."
cd st
sudo make clean install
cd ..
rm st -rf
echo "st installed succesfully!"

echo "Cloning github.com/jguer/yay..."
git clone https://github.com/Jguer/yay
echo "Running sudo make clean install..."
cd yay
sudo make clean install
cd ..
rm st -rf
echo "yay installed succesfully!"

echo "Cloning github.com/zma/usefulscripts..."
git clone https://github.com/zma/usefulscripts $homepath/
echo "Cloning succesfull!"


echo "Copying 50-mouse-acceleration.conf to /etc/X11/xorg.conf.d/50-mouse-acceleration.conf"
sudo cp configs/50-mouse-acceleration.conf
echo "Mouse configuration copyied succesfully!"


echo "Installation completed succesfully!"
exit 0
