#!/bin/bash
set -e 

read -p "Are you sure you want to procede? This script will most likely override many files on your system [y/n] " yn

if [ $yn != "y" ]; then
	echo "Aborting..."
	exit 0
fi
read -p "Your home directory (for example /home/johnsmith, notice there is no backslash at the end) : " homepath

if [ ! -d $homepath ]; then
	echo "Not a valid home directory. Aborting..."
	exit 0
fi

read -p "Are you sure $homepath is where you want to install this configuration? It may overwrite many files [y/n] " yn

if [ $yn != "y" ]; then
	echo "Aborting..."
	exit 0
fi

echo "Initiating package installation..."
sudo pacman -S pavucontrol pipewire pipewire-pulse pipewire-alsa pipewire-media-session pipewire-audio xrandr arandr picom firefox steam discord base-devel --needed
echo "Package installation succesfull!"

echo "Initiating configuration installation..."

echo "Copying .xinitrc to $homepath"
cp configs/.xinitrc $homepath/
echo ".xinitrc (X server configuration file) copied succesfully"

echo "Copying .bashrc to to $homepath"
cp configs/.bashrc $homepath/
echo ".bashrc copied succesfully!"

echo "Copying picom.conf to $homepath/.config"
cp configs/picom.conf $homepath/
echo "picom.conf (X11/ compositor configuration file) copied succesfully!"

echo "Copying wallpaper.jpg to $homepath/Pictures"
if [ ! -d $homepath/Pictures ]; then
	mkdir $homepath/Pictures
	echo "Created directory $homepath/Pictures"
fi
if [ ! -f $homepath/Pictures/wallpaper.jpg ]; then
	cp pictures/wallpaper.jpg $homepath/Pictures/
	echo "wallpaper.jpg copied succesfully!"
else
	echo "skipping copying wallpaper.jpg..."
fi

read -p "Do you want to modify your grub config? [y/n] " yn

if [ $yn == "y" ]; then
	echo "Copying grub to /etc/default/grub"
	sudo cp configs/grub /etc/default/
	echo "grub (grub configuration file) copied succesfully!"
	echo "Updating grub..."
	sudo update-grub
	echo "Grub updated succesfully!"
else 
	echo "Skipping...";
fi

echo "Cloning gitlab.com/mrhid3/dwm..."
git clone https://gitlab.com/mrhid3/dwm
echo "Runnning sudo make clean install..."
cd dwm
sudo make clean install
cd ..
rm dwm -rf
echo "dwm installed succesfully!"

echo "Cloning gitlab.com/mrhid3/dmenu"
git clone https://gitlab.com/mrhid3/dmenu
echo "Runnning sudo make clean install"
cd dmenu
sudo make clean install
cd ..
rm dmenu -rf
echo "dmenu installed succesfully!"

echo "Cloning gitlab.com/mrhid3/st..."
git clone https://gitlab.com/mrhid3/st
echo "Running sudo make clean install..."
cd st
sudo make clean install
cd ..
rm st -rf
echo "st installed succesfully!"

echo "Cloning gitlab.com/jguer/yay..."
git clone https://gitlab.com/Jguer/yay
echo "Running sudo make clean install..."
cd yay
sudo make clean install
cd ..
rm st -rf
echo "yay installed succesfully!"


echo "Copying 50-mouse-acceleration.conf to /etc/X11/xorg.conf.d/50-mouse-acceleration.conf"
sudo cp configs/50-mouse-acceleration.conf
echo "Mouse configuration copyied succesfully!"


echo "Installation completed succesfully!"
exit 0
