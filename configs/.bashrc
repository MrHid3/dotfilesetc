#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
alias gacp='git add * && git commit -m "update" && git push origin HEAD'
alias discord='discord --no-sandbox'
alias gits='gh repo sync --force'
alias syu='sudo pacman -Syu --noconfirm'

# startx on tty1
TTY="$(/usr/bin/tty | sed 's:.*/::')"
if [[ ! ${DISPLAY} && ${TERM} == 'linux' && ${TTY} == 'tty1' ]]; then
  unset TTY
  exec startx
fi

neofetch && 
/home/gabriel/usefulscripts/script/laptopkb/laptopkb-disable.sh &&
setxkbmap pl &&

# Ensure XDG_RUNTIME_DIR is set
unset XDG_RUNTIME_DIR
export XDG_RUNTIME_DIR=$(mktemp -d /tmp/$(id -u)-runtime-dir.XXX)
